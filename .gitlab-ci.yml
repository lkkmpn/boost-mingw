image: debian:latest

stages:
  - check-update
  - check-version
  - compile
  - upload
  - release

variables:
  PROJECT_API_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}"
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/boost-mingw-w64-x86-64-posix"

check-update-job:
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
  stage: check-update
  script:
    - apt update
    - apt install -y git openssh-client python3-pip python3-venv --no-install-recommends
    - python3 -m venv env
    - source env/bin/activate
    - python3 -m pip install beautifulsoup4 feedparser requests
    - python3 get_latest_version.py
    - eval $(ssh-agent -s)
    - echo "${SSH_PRIVATE_KEY}" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - git config --global user.name "${GIT_USERNAME}"
    - git config --global user.email "${GIT_EMAIL}"
    - git add version.json
    - git commit -m "Update version.json" || echo "Nothing to commit."
    - git remote rm origin && git remote add origin git@gitlab.com:$CI_PROJECT_PATH.git
    - git push origin HEAD:main

check-version-job:
  rules:
    - if: $CI_COMMIT_BRANCH && $CI_PIPELINE_SOURCE == "push"
      changes:
        - version.json
  stage: check-version
  script:
    - apt update
    - apt install -y python3-pip python3-venv --no-install-recommends
    - python3 -m venv env
    - source env/bin/activate
    - python3 -m pip install requests
    - python3 check_version.py
  artifacts:
    reports:
      dotenv: civars.env

compile-job:
  rules:
    - if: $CI_COMMIT_BRANCH && $CI_PIPELINE_SOURCE == "push"
      changes:
        - version.json
  stage: compile
  needs:
    - job: check-version-job
      artifacts: true
  script:
    - apt update
    - apt install -y bzip2 curl jq g++ g++-mingw-w64-x86-64-posix
    - ROOT=$(pwd)
    - mkdir ${BUILDDIR}
    - curl -OL ${URL}
    - tar xf ${DL_FILENAME}
    - DL_DIRNAME=$(tar tf ${DL_FILENAME} | head -1) || true
    - cd ${DL_DIRNAME}
    - 'echo "using gcc : mingw : x86_64-w64-mingw32-g++ ;" > user-config.jam'
    - ./bootstrap.sh
    - ./b2 -j 1 --user-config=user-config.jam --prefix=${ROOT}/${BUILDDIR} target-os=windows address-model=64 variant=release install || true
    - cd ..
    - tar cf ${TARFILE} ${BUILDDIR}
    - mkdir out
    - mv ${TARFILE} out
  artifacts:
    paths:
      - out/*
    expire_in: 1 hour

upload-job:
  rules:
    - if: $CI_COMMIT_BRANCH && $CI_PIPELINE_SOURCE == "push"
      changes:
        - version.json
  image: curlimages/curl:latest
  stage: upload
  needs:
    - job: check-version-job
      artifacts: true
    - job: compile-job
      artifacts: true
  script:
    - 'curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file out/${TARFILE} ${PACKAGE_REGISTRY_URL}/${VERSION}/${TARFILE}'

release-job:
  rules:
    - if: $CI_COMMIT_BRANCH && $CI_PIPELINE_SOURCE == "push"
      changes:
        - version.json
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  stage: release
  needs:
    - job: check-version-job
      artifacts: true
    - job: upload-job
  script:
    - echo "Releasing build..."
  release:
    tag_name: ${VERSION}
    description: "Boost ${VERSION} (x86_64-w64-mingw32)"
    assets:
      links:
        - name: ${TARFILE}
          url: ${PACKAGE_REGISTRY_URL}/${VERSION}/${TARFILE}
          filepath: /${TARFILE}
          link_type: package
