#!/usr/bin/env python3

import json
import os
import requests
import sys


def main():
    with open('version.json', 'r') as f:
        json_data = json.load(f)
    
    print(f'Boost version {json_data["version"]} was requested.')

    # check if this version already exists on GitLab
    headers = {'PRIVATE-TOKEN': os.environ['PAT']}
    url = f'{os.environ["PROJECT_API_URL"]}/releases/{json_data["version"]}'
    r = requests.get(url, headers=headers)
    if r.status_code == 200:
        print('This version is already available on GitLab. Cancelling this pipeline...')
        cancel_pipeline()
        sys.exit(0)
    elif r.status_code != 404:
        print(f'GitLab returned status code {r.status_code}. Probably bad news. GitLab response:')
        print(r.text)
        sys.exit(1)
    
    print('This version is not yet available on GitLab. Continuing.')

    # write variables to environment file to make them easier to handle
    ci_vars = {
        'VERSION': json_data['version'],
        'URL': json_data['url'],
        'BUILDDIR': f'boost-mingw-w64-x86-64-posix-{json_data["version"]}',
        'TARFILE': f'boost-mingw-w64-x86-64-posix-{json_data["version"]}.tar.bz2',
        'DL_FILENAME': os.path.basename(json_data['url'])
    }
    with open('civars.env', 'w') as f:
        f.write('\n'.join(f'{k}={v}' for k, v in ci_vars.items()))
        f.write('\n')
        

def cancel_pipeline():
    headers = {'PRIVATE-TOKEN': os.environ['PAT']}
    url = f'{os.environ["PROJECT_API_URL"]}/pipelines/{os.environ["CI_PIPELINE_ID"]}/jobs'
    jobs = requests.get(url, headers=headers).json()
    for job in jobs:
        if job['stage'] == 'compile':
            job_id = job['id']
            break

    # cancel this job
    url = f'{os.environ["PROJECT_API_URL"]}/jobs/{job_id}/cancel'
    requests.post(url, headers=headers)


if __name__ == '__main__':
    main()
