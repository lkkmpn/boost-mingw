#!/usr/bin/env python3

import json
import os
import re
import feedparser
import requests
from bs4 import BeautifulSoup


VERSION_REGEX = r'(?:\d+\.)+\d+'


def main():
    feed = feedparser.parse('https://www.boost.org/feed/downloads.rss')

    # get version
    version_string = feed.entries[0]['title']
    latest_version = re.findall(VERSION_REGEX, version_string)[0]

    # get url to download
    html = requests.get(feed.entries[0]['link']).text
    soup = BeautifulSoup(html, 'html.parser')
    table = soup.find('table', class_='download-table')
    links = table.find_all('a')
    for link in links:
        if link.get('href').endswith('.tar.bz2'):
            url = link.get('href')
            break
        
    print(f'Latest version: {latest_version}')
    print(f'Download link to latest version: {url}')
    
    # write info to file
    json_data = {
        'version': latest_version,
        'url': url
    }

    with open('version.json', 'w') as f:
        json.dump(json_data, f)


if __name__ == '__main__':
    main()
