# Boost cross-compiled for Mingw-w64

This repository is set up to cross-compile Boost on Linux for Mingw-w64.
The compiled files can be used in projects that are to be cross-compiled with
Mingw-w64 to create Windows builds that require the Boost library.

Cross-compilation is done using the latest versions of g++ and Mingw-w64
(specifically, using the POSIX threading model) offered by the latest stable
version of Debian at the time of building. Furthermore, only x64 builds are
offered. See
[the GitLab CI configuration](.gitlab-ci.yml) for more information.

For compiled builds, see
[the releases page](https://gitlab.com/lkkmpn/boost-mingw/-/releases). A
scheduled pipeline is set up to check for new Boost releases daily.

## Disclaimer

I am far from an expert in compiling software, especially cross-compilation.
The reason this repository exists is that I need this for a different project,
and found it useful to host the compilation scripts and compiled binaries in a
separate repository so that I can quickly download them in my build scripts.
Therefore, if there is anything wrong with these binaries, or they don't work
for your needs, I most likely cannot help you. Feel free to use the build
configuration from this repository to create your own builds.
